#include <iostream>
#include <conio.h>
using namespace std;
class Node {
  public:    string data;
    	    Node *next;
             Node() {};
             void SetData(string aData) { data = aData; };
             void SetNext(Node* aNext) { next = aNext; };
             string Data() { return data; };
             Node* Next() { return next; };
};

class List {
      Node *head;
      public:
             List() { head = NULL; };
             void Display();
             void Append(string data);
             void Remove(string data);
           	 string ViewData(int index);             
};

string List::ViewData(int index){
	Node* temp	=	head;
	int	Index	=	1;
	while (Index != index){
		temp =	temp->next;
		Index++;
	}
	if (temp) return temp->data;
	return NULL;
}


void List::Display() {
	int num = 0;
	Node* temp	=	head;
	while (temp != NULL){
		cout<<num+1<<"."<<temp->data<<endl;
		temp	=	temp->next;
		num++;
	}
	
}

void List::Append(string data) {
     Node* newNode = new Node();
     newNode->SetData(data);
     newNode->SetNext(NULL);
     Node *temp = head;
     if ( temp != NULL ) {
          while ( temp->Next() != NULL ) {
                temp = temp->Next();
          }
     temp->SetNext(newNode);
     }
     else {
          head = newNode;
     }
}

void List::Remove(string data) {
     Node *temp = head;
     if ( temp == NULL )
          return;
     if ( temp->Next() == NULL ) {
          delete temp;
          head = NULL;
     }
     else {
          Node *prev;
          do {
              if ( temp->Data() == data ) break;
              prev = temp;
              temp = temp->Next();
          } while ( temp != NULL );
     prev->SetNext(temp->Next());
     delete temp;
     }
}

int main(){
	List list, lib;
	int num;
	string music;
	int i;
	int x;
	
	lib.Append("Beautiful (Bazzi)");
	lib.Append("Just so you know (Jesse McCartney)");
	lib.Append("Narda (Kamikazee)");
	lib.Append("Roses (Chris Brown)");
	lib.Append("Huling Sayaw (Kamikazee)");
	

	Main_menu:
	cout <<"	1. Add Music:  "<< endl;
	cout <<"	2. Remove music: "<< endl;
	cout <<"	3. Display Playlist: " <<endl;
	cout <<"	4. View Library list: "<< endl; 	
	cout <<"	5. Add music to Library: " << endl;
	cout <<"	6. Edit music data(cannot edit built-in music): " << endl;
	cout <<"Select the number of your choice: ";
	cin>> num;
	
	if(cin.fail())
		{
			cin.clear();
			cin.ignore();
		}

		else if (num == 1)
		{
			system ("CLS");
	    	lib.Display();			
            cout<<"Enter the number of song you want to add: ";
            cin>> i;
            list.Append(lib.ViewData(i));
			cout<< "Playlist:"<<endl;						
            list.Display();
		}
	else if (num == 3)
	{
		system("CLS");
		lib.Display();
		cout <<"Select the song you want to play:  "<<endl;
		cin>> i;
		cout<<"Now playing: "<<lib.ViewData(i)<<endl;	
									
				}
				
	else if (num == 4)
	{
		cout << "Song Library: " << endl;
		lib.Display();
	}
				
	else if (num == 6)
	{
		system ("CLS");
		lib.Display();
		cout<< "Select the song title you want to edit : "<< endl;
		cin >> music;
		cout << "Enter the new song title:";
		cin >> music;
		lib.Append(music);
		lib.Display();
	}
	
	else if (num == 5)
	{
		system("CLS");
		cout<< "Enter the Song title: "<<endl;
		cin.ignore();
		getline(cin,music);
		lib.Append(music);
	}
	
	
	if (num == 2)
	{  
		system("CLS");
		lib.Display();
		cout<<"\Select the song title you want to remove: ";
        cin>> music;
        lib.Remove(music);
	}
	
	else 
	{
		
	}			
	cout<< "\n\n\nPress any key to go back.";
		getch();
		goto Main_menu;
}
